# QKeep (qkeep)

QKeep with Quasar Framework

## Features

### Drawer

1. Generate fake tags with faker library

### Create note

1. Create note component
1. Use mixin handle events

### Notes

1. Use component QCheckbox
1. Use component QChip
1. Generate fake data note
1. Notes with different settings

### Dialog new note

1. Use components: QDialog, QMenu, QCardActions
1. Install directives ClosePopup
1. Dinamic load component with <component>

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
