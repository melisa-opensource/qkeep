export function records (state) {
  return state.records
}

export function pagination (state) {
  return state.pagination
}
