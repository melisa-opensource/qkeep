import faker from 'faker/locale/es_MX'
import {
  generateTitle,
  generateContent,
  generateColor,
  generateTags,
  generateContentList
} from './fake'

export function paging (context) {
  const notes = Array.from(Array(20), (_, x) => {
    let note = {
      id: ++x,
      title: generateTitle(),
      content: generateContent(),
      color: generateColor(),
      tags: generateTags(),
      is_list: false
    }
    if (!note.title && !note.content) {
      const titleOrContent = faker.random.boolean()
      if (titleOrContent) {
        note.title = generateTitle(true)
      } else {
        note.content = generateContent(true)
      }
    }
    if (note.content && faker.random.boolean()) {
      note.is_list = true
      note.content = generateContentList(note.content)
    }
    return note
  })
  context.commit('setRecords', notes)
  return notes
}
