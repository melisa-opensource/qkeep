export default {
  records: [],
  pagination: {
    page: 1,
    rowsPerPage: 20
  }
}
