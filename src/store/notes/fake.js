import faker from 'faker/locale/es_MX'

export const generateTitle = (force) => {
  let withTitle = faker.random.boolean()
  if (force) {
    withTitle = true
  }
  return withTitle ? faker.name.jobTitle() : null
}

export const generateContentList = (content) => {
  const items = content.split(/\./)
  return items.map((item, i) => {
    return {
      id: ++i,
      content: item,
      completed: faker.random.boolean()
    }
  }).filter((el) => {
    return el.content
  })
}

export const generateContent = (force) => {
  let withContent = faker.random.boolean()
  if (force) {
    withContent = true
  }
  if (!withContent) {
    return null
  }
  const totalSentences = faker.random.number({
    min: 1,
    max: 5
  })
  return faker.lorem.sentences(totalSentences)
}

export const generateColor = () => {
  if (!faker.random.boolean()) {
    return null
  }
  const colors = [
    'white',
    'red-2',
    'orange-2',
    'green-2',
    'teal-2',
    'blue-2',
    'grey-2',
    'yellow-2'
  ]
  return colors[faker.random.number(colors.length - 1)]
}

export const generateTags = () => {
  const witTags = faker.random.boolean()
  if (witTags) {
    return []
  }
  const maxTags = faker.random.number(6)
  return Array.from(Array(maxTags), (_, x) => {
    return {
      id: ++x,
      name: faker.commerce.department()
    }
  })
}
