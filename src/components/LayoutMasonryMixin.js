// credits
// https://medium.com/@andybarefoot/a-masonry-style-layout-using-css-grid-8c663d355ebb
export default {
  data () {
    return {
      wrapperCls: '.layout-masonry > .row',
      itemWrapperCls: 'item-grid',
      itemCls: '.q-card'
    }
  },
  methods: {
    resizeGridItem (item) {
      const grid = document.querySelector(this.wrapperCls)
      const rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'))
      const rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'))
      const itemChildren = item.querySelector(this.itemCls)
      const rowSpan = Math.ceil((itemChildren.getBoundingClientRect().height + rowGap) / (rowHeight + rowGap))
      item.style.gridRowEnd = `span ${rowSpan}`
      // fix height item children
      itemChildren.style = 'height: 100%'
    },
    resizeAllGridItems () {
      const allItems = document.getElementsByClassName(this.itemWrapperCls)
      for (let x = 0; x < allItems.length; x++) {
        this.resizeGridItem(allItems[x])
      }
    },
    resizeInstance (instance) {
      const item = instance.elements[0]
      this.resizeGridItem(item)
    },
    installLayoutMasonry (wrapperCls, itemWrapperCls, itemCls) {
      this.wrapperCls = wrapperCls || this.wrapperCls
      this.itemWrapperCls = itemWrapperCls || this.itemWrapperCls
      this.itemCls = itemCls || this.itemCls
      window.onload = this.resizeAllGridItems()
      window.addEventListener('resize', this.resizeAllGridItems)
    },
    uninstallLayoutMasonry () {
      window.removeEventListener('resize', this.resizeAllGridItems)
    }
  }
}
