import LayoutMasonryMixin from './LayoutMasonryMixin'

export default {
  mixins: [
    LayoutMasonryMixin
  ],
  created () {
    this.onRequest()
  },
  beforeDestroy () {
    this.uninstallLayoutMasonry()
  },
  computed: {
    notes () {
      return this.$store.getters['notes/records']
    },
    pagination () {
      return this.$store.getters['notes/pagination']
    }
  },
  mounted () {
    setTimeout(() => this.installLayoutMasonry(), 1000)
  },
  methods: {
    onRequest () {
      this.$store.dispatch('notes/paging')
    },
    onRemoveTag (tag) {
      console.log(tag)
    }
  }
}
