export default {
  data () {
    return {
      isShow: this.show,
      form: {
        title: '',
        content: ''
      }
    }
  },
  watch: {
    show (value) {
      this.isShow = value
    },
    isShow (value) {
      this.$emit('update:show', value)
    }
  },
  methods: {
    onClickBtnClose () {
      this.isShow = false
    },
    onShowDialog () {
      this.$refs.txtContent.focus()
    }
  }
}
