export default {
  data () {
    return {
      showDialog: false
    }
  },
  methods: {
    onNewNote () {
      this.showDialog = true
    },
    getComponentDialogNote () {
      return import('src/components/DialogNote')
    }
  }
}
